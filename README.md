yii2sassutils
=================

SASS Утилиты + регистрация css классов

## Установка

Предпочтительный способ установить это расширение через композитор. [composer](http://getcomposer.org/download/).

Необходимо добавить

```
"studiosite/yii2sassutils": "*"
```

в секции ```require``` `composer.json` файла.


## Использование

#### Заполнение (padding)

В пакет входит регистрация классов ```padding-X``` ```padding-top-X``` ```padding-left-X``` ```padding-right-X``` ```padding-bottom-X``` Где ```X``` - размер отступа заполнения в ```px```

#### Отступ (margin)

По аналогии с заполнением будет зарегистрированы отступы ```margin-X``` ```margin-top-X``` ```margin-left-X``` ```margin-right-X``` ```margin-bottom-X``` Где ```X``` - размер отступа в ```px```

#### Прозрачность (opacity)

21 класс от 0 с шагом 5. `opacity-X`, где `x` - процент видимости