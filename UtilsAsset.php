<?php

namespace studiosite\yii2sassutils;

use Yii;
use yii\web\AssetBundle;

/**
 * Scss утилиты. Помошь в верстке
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 *
 * @property string $baseUrl
 * @property string $sourcePath
 * @property array $css
 * @property array $js
 * @property array $depends
 */
class UtilsAsset extends AssetBundle
{
	/**
    * @var string Альяс пути где находятся асетсы
    */
	public $baseUrl = '@web';

	/**
    * @var array Исходный путь
    */
    public $sourcePath = '@studiosite/yii2sassutils/assets';

    /**
    * Инициализация
    */
    public function init()
    {
        Yii::setAlias('@studiosite/yii2sassutils', __DIR__);
    }

    /**
    * @var array Список файлов стилей по порядку подключения
    */
    public $css = [
        'margin-padding.scss',
        'opacity.scss'
    ];

    /**
    * @var array Список файлов JS файлов по порядку подключения
    */
    public $js = [
    ];
}
